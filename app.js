const fastify = require('fastify')()
const mysql = require('mysql')

const con = mysql.createConnection({
    host: "localhost",
    user: "root",
    database: 'maroto'
})

fastify.get('/actores', (request, reply) => {
    con.connect((err) => {
        if (err) return reply.send(err)
        
        con.query('select * from actors', (err, result) => {
            if (err) return reply.send(err)
            
            reply.send(result)
            con.end()
        })
    })
})

fastify.get('/actores/:id', (request, reply) => {
    con.connect((err) => {
        if (err) return reply.send(err)
        
        con.query(`select * from actors where id=${request.params.id}`, (err, result) => {
            if (err) return reply.send(err)
            
            reply.send(result)
            con.end()
        })
    })
})

fastify.delete('/actores/:id', (request, reply) => {
    con.connect((err) => {
        if (err) return reply.send(err)
        
        con.query(`delete from actors where id=${request.params.id}`, (err, result) => {
            if (err) return reply.send(err)
            
            reply.send(result)
            con.end()
        })
    })
})


fastify.post('/actores', (request, reply) => {
    con.connect((err) => {
        if (err) return reply.send(err)
        
        con.query(`insert into actors (name, level) values ("${request.body.name}", ${request.body.level})`, (err, result) => {
            if (err) return reply.send(err)
            
            reply.send(result)
            con.end()
        })
    })
})


fastify.listen(3000, '127.0.0.1', (err) => {
  if (err) throw err
  console.log(`server listening on ${fastify.server.address().port}`)
})